import express, { Request, Response } from 'express';
import multer from 'multer';
import cors from 'cors';
import iconv from 'iconv-lite';
import path from 'path';
import { createClient } from 'redis';
import fs from 'fs';
import 'dotenv/config';

function generateRandomFileName(extension: string): string {
    let randomName = Math.random().toString(36).substring(2, 15);
    let fileName = `${randomName}.${extension}`;
    return fileName;
}

async function setKeyOnRedis(key: string, value: any) {
    const client = createClient({
        url: process.env.REDIS_URL,
        password: process.env.REDIS_PASS
      });
    
    client.on('error', err => console.log('Redis Client Error', err));
    await client.connect();
    const multi = client.multi();
    multi.set(key, JSON.stringify(value));
    await multi.exec();  
    await client.disconnect();
}

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/');
  },
  filename: (req, file, cb) => {
      setKeyOnRedis(file.originalname, {client: req.query.client,filename: file.originalname, status: 1});
    const newFileName = iconv.decode(Buffer.from(file.originalname, 'binary'), 'utf8')
    cb(null, newFileName);
  }
});

const upload = multer({ storage, 
  fileFilter: async (req, file, cb) => {
      let eventualError: any = null;
      try{
          const originalFileName: any = iconv.decode(Buffer.from(file.originalname, 'binary'), 'utf8');
          if(!originalFileName.includes("."))
            throw new Error("Arquivo sem extensão.");

          const [filename, extension] = originalFileName.split('.');
          if(extension !== "jpeg" && extension !== "jpg" && extension !== "png")
            throw new Error("Extensão de arquivo inválida.");

          file.originalname = generateRandomFileName(extension);
      }catch(err){
          console.log(err);
          eventualError = err;
      }finally{
          if (eventualError) cb(eventualError);
          else cb(null, true);
      }
  }
},);

const app = express();
app.use(cors({
  origin: '*'
}));
app.use(express.json());

app.get("/status", (req, res) => {
    res.json(Date.now())
})

app.get("/list", async (req, res) => {
    const client = createClient({url: process.env.REDIS_URL,password: process.env.REDIS_PASS});
    client.on('error', err => console.log('Redis Client Error', err));
    await client.connect();
  
    const keys = await client.keys("*");
    console.log("KEYS:", keys)
    if(keys.length === 0) return res.send(null);
  
    const data: any = {};
    const values = await client.mGet(keys);
    keys.forEach((key, index) => {
      data[key] = JSON.parse(values[index] as string);
    });
    return res.send(data);
})
  
app.post('/upload', async (req, res) => {   
  const {client} = req.query;
  if(!client) return res.status(400).send("Usuário não informado.");

    await upload.any()(req, res, async (err) => {
        if (err) {
            console.error("error");
            console.log(err);
            return res.status(400).json({ message: err.message });
        }
    });
    return res.json();
});

app.post("/remove", async (req: Request, res: Response) => {
    const {filename} = req.body;
    const client = createClient({
      url: process.env.REDIS_URL,
      password: process.env.REDIS_PASS
    });
  
    client.on('error', err => console.log('Redis Client Error', err));
    await client.connect();
    
    const redisResponse = await client.get(filename);
  
    if(!redisResponse)
      return res.status(400).send("Arquivo não encontrado.");
  
    await client.del(filename);
    await client.disconnect();
  
    const filePath = path.join(path.dirname(__dirname), 'uploads', filename);
    console.log("FILE PATH:", filePath)
    
    fs.unlink(filePath, (err) => {
      if (err) {
        console.error(err);
        return res.status(500).json({ message: 'Failed to delete the file.' });
      }
      return res.status(200).json({ message: 'File successfully deleted.' });
    });
})
  

app.put("/register-callback",async (req: Request, res: Response) => {
    const {url} = req.body;
    console.log("Registrando novo callback:", url)
    await setKeyOnRedis("callback", {url: url});
    res.json();
})
  
  
app.listen(4000, () => console.log('Server Started...'));
