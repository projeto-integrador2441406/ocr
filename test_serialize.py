import os
from app.util import SerializationUtil

filename = 'image1'
output_dir = 'app/serialized_ocr_results'

dict_data = {
    'key1': 'value1',
    'key2': 'value2',
    'key3': 'value3'
}


SerializationUtil.serialize(dict_data, output_dir, filename)

result = SerializationUtil.deserialize(os.path.join(output_dir, filename + ".pkl"))

print(result)
# print(os.path.join(output_dir, (filename + ".pkl")))
# # Pickle the dictionary
# with open(os.path.join(output_dir, filename + ".pkl"), 'wb') as f:
#     pickle.dump(dict_data, f)


# Unpickle the dictionary
# with open(os.path.join(output_dir, (filename + ".pkl")), 'rb') as f:
#     loaded_dict_data = pickle.load(f)
#
# print(loaded_dict_data)