import os
import re
import json
from util.serialization import SerializationUtil
from model.product import Product


def extract_cf_info(ocr_result):
    #CF (Cupom Fiscal) header tokens
    products_header = ['item', 'codigo', 'descricao', 'quantidadde', 'qtde', 'valor total']
    #CF supermarket tokens
    sao_luiz_tokens = ['mercadinho', 'mercadinhos', 'sao luiz', 'são luiz', 'são luis']
    pao_acucar_tokens = ['pao de acucar', 'pão de acucar', 'pão de acucar', 'pao de açucar']
    product_line_tokens = ['kg', 'un', 'ka', 'vn']

    ocr_content = ocr_result['Blocks']
    doc_lines = list(map(lambda x: x['Text'], list(filter(lambda x: x['BlockType'] == 'LINE', ocr_content))))

    doc_lines_tmp = "\n".join(doc_lines)

    potential_prod_lines = re.findall(r"\b\d{3}\b\s+\d{3}", doc_lines_tmp, re.MULTILINE)
    potential_products = []
    check_products = False
    supermarket_name = None
    for string in doc_lines:
        parsed_string = string.lower()

        if supermarket_name is None:
            if any(sup_mark_name in parsed_string for sup_mark_name in sao_luiz_tokens):
                supermarket_name = parsed_string
            elif any(sup_mark_name in parsed_string for sup_mark_name in pao_acucar_tokens):
                supermarket_name = parsed_string

        found_header = any(header_token in parsed_string for header_token in products_header)

        if found_header:
            check_products = True
            continue

        if not check_products:
            continue

        splited_string = parsed_string.split(" ")
        if any(pot_prod_line in parsed_string for pot_prod_line in potential_prod_lines):
            potential_products.append(string)
        elif any(item in splited_string for item in product_line_tokens):
            potential_products.append(string)

    final_products = []
    #Get name, price and quantity from potential product
    for pot_product in potential_products:
        splited = []
        if "kg" in pot_product:
            splited = pot_product.split("kg")
        elif "un" in pot_product:
            splited = pot_product.split("un")
        elif "ml" in pot_product:
            splited = pot_product.split("ml")
        else:
            continue
        qty = splited[0].strip().split()[-1].replace(".",",")
        price = splited[1].strip().split()[-1].replace(".",",")
        name = " ".join(re.findall(r'\b[A-Z][a-zA-Z]*\b', pot_product)).replace(" X ", "")
        final_products.append(Product(name, qty, price).to_dict())

    return json.dumps({"supermarket": supermarket_name, "products": final_products})


# filename = 'nota_2'
# output_dir = 'serialized_ocr_results'
# ocr_result = SerializationUtil.deserialize(os.path.join(output_dir, filename + ".pkl"))
#
# print(extract_cf_info(ocr_result))


