import json
import os
import redis
import time
import requests
from datetime import datetime
from dotenv import load_dotenv
from util.enum.file_status_enum import FileStatus
from mr_robot_ocr import TesseractOCR
from extract_info import extract_cf_info
from pymongo import MongoClient
load_dotenv()

print(os.getenv("REDIS_HOST"))

r = redis.Redis(host=os.getenv("REDIS_HOST"), port=os.getenv('REDIS_PORT'), db=os.getenv('REDIS_DB'), decode_responses=True, password=os.getenv("REDIS_PASS"))


def get_db():
    client = MongoClient(os.getenv("MONGO_STR_CONN"))
    db = client['cestaCheia']
    return db

def main():
    url_callback = None
    print("Application started")
    while True:
        files = []
        pendent_files = []
        file_keys = r.keys("*")

        for file_key in file_keys:
            msg_body = json.loads(r.get(file_key))
            if 'status' in msg_body and msg_body['status'] == FileStatus.NEW.value:
                pendent_files.append({"filename": file_key, "client": msg_body['client']})

        if len(pendent_files) == 0:
            time.sleep(5)
            continue

        for file in pendent_files:
            try:
                print(f"Processando arquivo {file['filename']}")
                file_path = os.path.join(os.getenv('RESOURCES_DIR'), file['filename'])
                ocr_result = TesseractOCR().read(document_path=file_path)
                extracted_info = json.loads(extract_cf_info(ocr_result))
                r.delete(file['filename'])
                print(extracted_info)
                print("Saving result on DB...")
                db = get_db()
                extracted_info['verified'] = False
                extracted_info['client'] = file['client']
                extracted_info['createdAt'] = datetime.now()
                db['receipts'].insert_one(extracted_info)
                print("Result saved on DB")
            except Exception as e:
                print("######Erro######")
                print(str(e))
                r.delete(file['filename'])




if __name__ == "__main__":
    main()
