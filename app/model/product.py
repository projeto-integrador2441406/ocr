

class Product:
    def __init__(self, name, qty, price):
        self.name = name
        self.qty = qty
        self.price = price

    def to_dict(self):
        return {"name": self.name, "quantity": self.qty, "price": self.price}
