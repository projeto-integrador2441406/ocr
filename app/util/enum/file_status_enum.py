from enum import Enum


class FileStatus(Enum):
    NEW = 1
    IN_PROGRESS = 2
    DONE = 3
    ERROR = 4
