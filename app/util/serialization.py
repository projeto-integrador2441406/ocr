import os
import pickle


class SerializationUtil:
    def __init__(self):
        pass

    @staticmethod
    def serialize(_dict, output_dir: str, filename: str):
        with open(os.path.join(output_dir, filename + ".pkl"), 'wb') as f:
            pickle.dump(_dict, f)

    @staticmethod
    def deserialize(filepath: str):
        with open(filepath, 'rb') as f:
            loaded_dict_data = pickle.load(f)
        return loaded_dict_data
